import React from 'react'
import AppRouter from '_router'
import 'antd/dist/antd.css'
const App = () => {
  return <AppRouter/>
}

export default App