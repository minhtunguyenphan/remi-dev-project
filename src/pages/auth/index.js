//Route
import AuthRoute from '_router/AuthRoute'

//Layout
import PublicLayout from "_layout/public";

//Pages
import SignInPage from './SignInPage';
import SignUpPage from './SignUpPage';

const routeConfig = {
  layout: PublicLayout,
  route: AuthRoute
}

const AuthRoutes = [
  {
    ...routeConfig,
    title: "SignIn Page",
    component: SignInPage,
    path: '/sign-in',
  },
  {
    ...routeConfig,
    title: "SignUp Page",
    component: SignUpPage,
    path: "/sign-up"
  }
]

export default AuthRoutes