import ShareVideoPage from './ShareVideoPage'
import PrivateRoute from '_router/PrivateRoute'
import PrivateLayout from '_layout/private'
const routeConfig = {
  layout: PrivateLayout,
  route: PrivateRoute
}

const PrivateRoutes = [
  {
    ...routeConfig,
    title: 'ShareVideo Page',
    component: ShareVideoPage,
    path: '/share-video',
  }
]

export default PrivateRoutes