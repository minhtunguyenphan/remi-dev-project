const path = require("path")
const webpack = require('webpack')
const Dotenv = require('dotenv-webpack')
module.exports = {
  entry: "./src/index.js",
  module: {
    rules: [
      {
        test: /\.html$/i,
        loader: 'html-loader'
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          // Creates `style` nodes from JS strings
          'style-loader',
          // Translates CSS into CommonJS
          'css-loader',
          // Compiles Sass to CSS
          'sass-loader'
        ]
      },
      {
        test: /\.svg$/,
        use: [
          {
            loader: 'svg-url-loader',
            options: {
              limit: 10000
            }
          }
        ]
      },
      {
        test: /\.(woff(2)?|ttf|eot|otf)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: './font/[hash].[ext]',
              mimetype: 'application/font-woff'
            }
          }
        ]
      },
      {
        test: /\.mjs$/,
        include: /node_modules/,
        type: "javascript/auto"
      },
      {
        exclude: [
          /\.html$/,
          /\.(js|jsx)$/,
          /\.css$/,
          /\.scss$/,
          /\.json$/
          // /\.bmp$/,
          // /\.gif$/,
          // /\.jpe?g$/,
          // /\.png$/,
        ],
        test: /\.(png|jpe?g|gif)$/i,
        loader: 'file-loader',
        options: {
          name: 'static/media/[name].[hash:8].[ext]'
        }
      }
    ]
  },
  resolve: {
    extensions: ["*", ".webpack.js", ".web.js", ".mjs", ".js", ".jsx"],
    alias: {
      _utils: path.resolve(__dirname, 'src/utils'),
      _components: path.resolve(__dirname, 'src/components'),
      _pages: path.resolve(__dirname, 'src/pages'),
      _configs: path.resolve(__dirname, 'src/configs'),
      _constants: path.resolve(__dirname, 'src/constants'),
      _api: path.resolve(__dirname, 'src/api'),
      _src: path.resolve(__dirname, 'src'),
      _helpers: path.resolve(__dirname, 'src/helpers'),
      _layout: path.resolve(__dirname, 'src/layout'),
      _router: path.resolve(__dirname, 'src/router'),
      _styles: path.resolve(__dirname, 'src/styles'),
      _static: path.resolve(__dirname, 'public/static'),
      _store: path.resolve(__dirname, 'src/store'),
      _assets: path.resolve(__dirname, 'src/assets'),
      _i18n: path.resolve(__dirname,'src/i18n'),
      _services: path.resolve(__dirname, 'src/services')
    }
  },
  output: {
    path: path.resolve(__dirname, "dist/"),
    publicPath: "/dist/",
    filename: "bundle.js"
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new Dotenv()
  ]
}